#####  create 2 ec2 instances making use of count / for each
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr


  tags = {
    Name = "koji"
  }
}

locals {
  aws_vpc = aws_vpc.main.id
  az =  data.aws_availability_zones.available.names
}

resource "aws_subnet" "public_subnet" {
  count = length(var.public_subnet_cidr)
  vpc_id     = local.aws_vpc
  cidr_block = var.public_subnet_cidr[count.index]
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}


resource "aws_subnet" "private_subnet" {
  count = length(var.private_subnet_cidr)
  vpc_id     = local.aws_vpc
  cidr_block = var.private_subnet_cidr[count.index]
  map_public_ip_on_launch = false
  availability_zone = local.az[count.index]

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}

resource "aws_instance" "web" {
  count = length(var.private_subnet_cidr)
  ami           = data.aws_ami.linux.id
  instance_type = var.instance_type
  key_name = data.aws_key_pair.key.key_name
  subnet_id = element([aws_subnet.private_subnet[count.index]])
  
  tags = {
    Name = "HelloWorld"
  }
}










