data "aws_ami" "linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_key_pair" "key" {
  key_name           = var.key_name
  include_public_key = true

}

data "aws_availability_zones" "available" {
  state = "available"
}
