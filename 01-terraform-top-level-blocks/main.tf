resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.vpc_cidr

 tags = {
   Name = "kojitechs-vpc"
 }
}

 resource "aws_subnet" "subnet_1" {

   cidr_block = var.subnet_1_cidr
   vpc_id = local.vpc_id
  availability_zone = data.aws_availability_zones.available.names[0]
   tags = {
     Name = "subnet_1"
   }
 }

  resource "aws_subnet" "subnet_2" {

   cidr_block = var.subnet_2_cidr
   vpc_id = local.vpc_id
  availability_zone = data.aws_availability_zones.available.names[1]
   tags = {
     Name = "subnet_2"
   }
 }

 resource "aws_instance" "web" {
  ami           = data.aws_ami.ami.id
  instance_type = "t2.micro"
  #subnet_id = aws_subnet.subnet_1.id
  

  tags = {
    Name = "web"
  }
}
