#########PARENT MODULE FOR S3


variable "bucket_name" {
  type = string
  description = "s3 bucket name"
}

variable "tags" {
    type = map
    description = "tag value"
    default = {}
}

variable "versioning" {
  tyoe = "string"
  description = "do you want to enable versioning?"
  #default [Enabled, Disabled, Suspend]
}