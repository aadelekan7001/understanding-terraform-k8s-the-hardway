terraform {
    required_version = ">=1.1.0"

  #     backend "s3" {
  #   bucket = "adelekan-state-s3"
  #   key = "path/env"
  #   region = "us-east-1"
  #   dynamodb_table = "terraform-lock"
  #   encrypt = true
  # }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.57.0"
    }
  }
}