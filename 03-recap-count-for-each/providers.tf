provider "aws" {
  region = "us-east-1"
  profile = "default" //if your profile is default, don't specify
}