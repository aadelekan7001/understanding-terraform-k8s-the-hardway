#####  create 2 ec2 instances making use of count / for each
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr


  tags = {
    Name = "koji-${terraform.workspace}"
  }
}

resource "aws_subnet" "public_subnet" {
  count = length(var.subnet_1_cidr)
  vpc_id     = local.aws_vpc
  cidr_block = var.subnet_1_cidr[count.index]
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}


resource "aws_subnet" "private_subnet" {
  count = length(var.subnet_2_cidr)
  vpc_id     = local.aws_vpc
  cidr_block = var.private_subnet_cidr[count.index]
  map_public_ip_on_launch = false
  availability_zone = local.az[count.index]

  tags = {
    Name = "public-subnets-${count.index + 1}"
  }
}

resource "aws_instance" "web" {
  count = 2
  ami           = data.aws_ami.linux.id
  instance_type = "t3.micro"
  key_name = data.aws_key_pair.key.key_name
  subnet_id = aws_subnet.public_subnet[count.index].id
  
  tags = {
    Name = "HelloWorld"
  }
}










