
variable "public_subnet_cidr" {
  description = "value for sunbet CIDR"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
}

variable "private_subnet_cidr" {
  description = "value for sunbet2 CIDR"
  type        = list(any)
  default     = ["10.0.7.0/24", "10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
}

variable "vpc_cidr" {
  description = "value for vpc CIDR"
  type        = string
  default     = "10.0.0.0/16"
}