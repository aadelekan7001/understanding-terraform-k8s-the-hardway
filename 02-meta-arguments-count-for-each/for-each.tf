// FOR EACH FOR META ARGUMENTS


resource "aws_vpc" "for_each_vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "kojitechs-vpc"
  }
}

# locals {
#   public_subnet = {
#       pub_subnet_1 = {
#           cidr_block = "10.0.0.0/24"
#           az = "us-east-1a"
#       }

#        pub_subnet_2 = {
#           cidr_block = "10.0.2.0/24"
#           az = "us-east-1b"
#       }
#   }
# }


# variable "public_subnets" {
#   type        = map(any)
#   description = "objects of public_subnets_to_be_created"
#   default = {
#     public_subnet_1 = {
#       cidr_block = "10.0.0.0/24"
#       az         = "us-east-1a"
#     }
#   }
#   # public_subnet_2 = {
#   #     cidr_block = "10.0.2.0/24"
#   #     az         = "us-east-1b"
#   #   }
#   # }



resource "aws_subnet" "public_subnets_for_each" {
  //count                   = length(var.public_subnet_cidr)
  for_each = var.public_subnets


  cidr_block              = each.value.cidr_block #remain the same
  vpc_id                  = local.vpc_id          #remain the same
  map_public_ip_on_launch = true
  availability_zone       = each.value.az

  tags = {
    Name = each.key
  }
}




# resource "aws_subnet" "private_subnets_for_each" {
#   //count             = length(var.private_subnet_cidr)
#   cidr_block        = var.private_subnet_cidr[count.index]
#   vpc_id            = local.vpc_id
#   availability_zone = data.aws_availability_zones.available.names[count.index]
#   tags = {
#     Name = "private-subnets-${count.index + 1}"
#   }
# }


# resource "aws_instance" "web" {
#   ami           = data.aws_ami.ami.id
#   instance_type = "t2.micro"
#   subnet_id = aws_subnet.public_subnets_for_each[0].id


#   tags = {
#     Name = "web"
#   }
# }


