resource "aws_instance" "jump-server" {

  ami                  = data.aws_ami.ami.id
  instance_type        = "t2.micro"
  subnet_id            = local.private_subnet[0]
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups      = [aws_security_group.static_sg.id] # 80 from who? alb
  user_data            = file("./templates/app1.sh")
  tags = {
    Name = "app1-${terraform.workspace}"
  }
    lifecycle {
    ignore_changes        = [security_groups] # this this to prevent our target group no to get destroyed
  }
}

