resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = var.vpc_cidr

 tags = {
   Name = "kojitechs-vpc"
 }
}

 resource "aws_subnet" "subnet_1" {

   cidr_block = var.subnet_1_cidr
   vpc_id = aws_vpc.kojitechs_vpc.id
  availability_zone = "us-east-1a"
   tags = {
     Name = "Main"
   }
 }

  resource "aws_subnet" "subnet_2" {

   cidr_block = var.subnet_2_cidr
   vpc_id = aws_vpc.kojitechs_vpc.id
  availability_zone = "us-east-1b"
   tags = {
     Name = "subnet_2"
   }
 }
