
variable "name" {
    description = "value for sunbet CIDR"
    type = string
    default = "10.0.1.0/24"
}
