resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = "10.0.0.0/16"

 tags = {
   Name = "kojitechs-vpc"
 }
}

 resource "aws_subnet" "subnet_1" {

   cidr_block = "10.0.1.0/24"
   vpc_id = aws_vpc.kojitechs_vpc.id
  availability_zone = "us-east-2a"
   tags = {
     Name = "Main"
   }
 }

  resource "aws_subnet" "subnet_2" {

   cidr_block = "10.0.2.0/24"
   vpc_id = aws_vpc.kojitechs_vpc.id
  availability_zone = "us-east-2b"
   tags = {
     Name = "subnet_2"
   }
 }
