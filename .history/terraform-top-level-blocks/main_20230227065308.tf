resource "aws_vpc" "kojitechs_vpc" {
  cidr_block = "10.0.0.0/16"

 tags = {
   Name = "kojitechs-vpc"
 }
}

# resource "aws_subnet" "main" {
#   vpc_id     = aws_vpc.kojitechs_vpc.id
#   cidr_block = "10.0.1.0/24"

#   tags = {
#     Name = "Main"
#   }
# }
