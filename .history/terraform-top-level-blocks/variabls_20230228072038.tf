
variable "subnet_1_cidr" {
    description = "value for sunbet CIDR"
    type = string
    default = "10.0.1.0/24"
}

variable "subnet_2_cidr" {
    description = "value for sunbet2 CIDR"
    type = string
    default = "10.0.2.0/24"
}

variable "vpc_cidr" {
    description = "value for vpc CIDR"
    type = string
    default = "10.0.0.0/16"
}