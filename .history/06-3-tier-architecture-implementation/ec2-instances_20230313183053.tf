// create public ec2 instance
resource "aws_instance" "bastion_host" {
  ami             = data.aws_ami.ami.id
  instance_type   = var.public_instance_type
  subnet_id       = aws_subnet.private_subnet[0].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  #subnet_id       = aws_subnet.public_subnet[0].id
  #key_name        = "koji"
  #security_groups = [aws_security_group.bastion_sg.id]

  tags = {
    Name = "private-instance(2)-${terraform.workspace}"
  }
}



// create PRIVATE ec2 instance
resource "aws_instance" "public_instance" {
  ami             = data.aws_ami.ami.id
  instance_type   = var.private_instance_type
  subnet_id       = aws_subnet.private_subnet[0].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  #key_name        = "koji"
  #security_groups = [aws_security_group.private_sg.id]

  tags = {
    Name = "private-instance-${terraform.workspace}"
  }
}