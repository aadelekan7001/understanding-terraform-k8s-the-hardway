

resource "aws_db_instance" "registration_app_db" {
  allocated_storage    = 20
  db_name              = "webappdb"  #application team (webappdb)
  engine               = "mysql"

  instance_class       = var.instance_class
  username             = var.db_username
  password             = "foobarbaz"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}