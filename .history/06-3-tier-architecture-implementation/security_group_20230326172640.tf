# //CREATE SG FOR BASTION

# resource "aws_security_group" "bastion_sg" {
#   name        = "bastion-sg-${terraform.workspace}"
#   description = "Allow SSH inbound traffic"
#   vpc_id      = local.vpc_id

#   ingress {
#     description = "allow ingress on port 22"
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["73.8.229.8/32"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "bastion-sg"
#   }
# }

// CREATE PRIVATE SG

resource "aws_security_group" "private_sg" {
  name        = "private_sg-${terraform.workspace}"
  description = "Allow SSH from public sg on port 22"
  vpc_id      = local.vpc_id



  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}
// CREATE  PRIVATE SG RULE

# resource "aws_security_group_rule" "allow_ssh_from_bastion_sg" {
#   type      = "ingress"
#   from_port = 22
#   to_port   = 22
#   protocol  = "tcp"

#   security_group_id        = aws_security_group.private_sg.id
#   source_security_group_id = aws_security_group.bastion_sg.id // reference the source IG.
# }





resource "aws_security_group" "app1" {
  name        = "app1-${terraform.workspace}"
  description = "Allow SSH inbound traffic"
  vpc_id      = local.vpc_id

  ingress {
    description = "allow ingress on port 22"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    # cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "app1-${terraform.workspace}"
  }
}



# //CREATE SG FOR ALB

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg-${terraform.workspace}"
  description = "Allow port 80/443 inbound traffic"
  vpc_id      = local.vpc_id

  ingress {
    description = "allow ingress on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


ingress {
    description = "allow ingress on port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-sg-${terraform.workspace}"
  }
}
