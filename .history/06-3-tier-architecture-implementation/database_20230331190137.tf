
locals {
  db_credentials = {
      endpoint = aws_db_instance.registration_app_db.address
      db_username = var.db_username
      db_name = var.db_name
      port = var.port
      password = random_password.password.result
  }
  
}


resource "random_password" "password" {
  length           = 16
  special          = false
  
}



resource "aws_db_subnet_group" "mysql_subnetgroup" {
  name       = "registrationapp_subnetgroup-${terraform.workspace}"
  subnet_ids = [aws_subnet.frontend.id, aws_subnet.backend.id]

  tags = {
    Name = "registrationapp_subnetgroup-${terraform.workspace}"
  }
}


resource "aws_db_instance" "registration_app_db" {
  allocated_storage    = 20
  db_name              = var.db_name  #application team (webappdb)
  engine               = "mysql"

  instance_class       = var.instance_class
  username             = var.db_username
  port = var.port
  password             = random_password.password.result
  db_subnet_group_name = 
  skip_final_snapshot  = true
}