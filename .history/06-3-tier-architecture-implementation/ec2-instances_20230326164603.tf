// create public ec2 instance
resource "aws_instance" "app_1" {
    count = length(local.priv_sub)

  ami             = data.aws_ami.ami.id
  instance_type   = var.public_instance_type
  subnet_id       = local.priv_sub[keys(local.priv_sub)[count.index]].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data = (app1.sh)
  #subnet_id       = aws_subnet.public_subnet[0].id
  #key_name        = "koji"
  #security_groups = [aws_security_group.bastion_sg.id]

  tags = {
    Name = "private-instance(2)-${terraform.workspace}"
  }
}



# // create PRIVATE ec2 instance
resource "aws_instance" "app_2" {
  ami             = data.aws_ami.ami.id
  instance_type   = var.private_instance_type
  subnet_id       = local.priv_sub[keys(local.priv_sub)[count.index]].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  #key_name        = "koji"
  #security_groups = [aws_security_group.private_sg.id]

  tags = {
    Name = "app-2-${terraform.workspace}"
  }
}