resource "aws_lb" "this" {
  name               = "web-al${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  //subnets            = local.pub_sub[keys(local.pub_sub)[count.index]].id
  subnets = aws_subnet.public_subnet.*.id




  tags = {
    Name = "web-al${terraform.workspace}"
  }
}


resource "aws_lb_target_group" "app1" {
  name        = "app1-tg-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    unhealthy_threshold = 3
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app1/metadata.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }
}



resource "aws_lb_target_group" "app2" {
  name        = "tf-example-lb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

   health_check {
    unhealthy_threshold = 3
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app2/metadata.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }
}


resource "aws_lb_target_group_attachment" "app1" {
  target_group_arn = aws_lb_target_group.app1
  target_id        = aws_instance.app1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "app2" {
  target_group_arn = aws_lb_target_group.app2.arn
  target_id        = aws_instance.app2.id
  port             = 80
}


resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.this.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}



resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.front_end.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_end.arn
  }
}