
locals {
  db_credentials = {
    endpoint    = aws_db_instance.registration_app_db.address
    db_username = var.db_username
    db_name     = var.db_name
    port        = var.port
    password    = random_password.password.result
  }

}

resource "aws_secretsmanager_secret" "this" {
  name = "kojitechs-mysql-secret-${terraform.workspace}"
  description = "secret to manage mysql superuser password"
}

# The map here can come from other supported configurations
# like locals, resource attribute, map() built-in, etc.
variable "example" {
  default = {
    key1 = "value1"
    key2 = "value2"
  }

  type = map(string)
}

resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.example.id
  secret_string = jsonencode(var.example)
}


resource "random_password" "password" {
  length  = 16
  special = false

}



resource "aws_db_subnet_group" "mysql_subnetgroup" {
  name       = "registrationapp_subnetgroup-${terraform.workspace}"
  subnet_ids = values(aws_subnet.database_subnet)[*].id
  tags = {
    Name = "registrationapp_subnetgroup-${terraform.workspace}"
  }
}


resource "aws_db_instance" "registration_app_db" {
  allocated_storage = 20
  db_name           = var.db_name #application team (webappdb)
  engine            = "mysql"

  instance_class         = var.instance_class
  username               = var.db_username
  port                   = var.port
  password               = random_password.password.result
  db_subnet_group_name   = aws_db_subnet_group.mysql_subnetgroup.name
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
}