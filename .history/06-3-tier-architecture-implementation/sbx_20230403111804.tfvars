vpc_cidr = "10.0.0.0/16"

//public_subnet_cidr = ["10.0.2.0/24", "10.0.4.0/24"]


private_subnet_cidr = {
  private_subnet_1 = {
    cidr_block = "10.0.1.0/24"
    az         = "us-east-1a"
  }

  private_subnet_2 = {
    cidr_block = "10.0.3.0/24"
    az         = "us-east-1b"
  }
}



database_subnet_cidr = {
  database_subnet_1 = {
    cidr_block = "10.0.5.0/24"
    az         = "us-east-1a"
  }

  database_subnet_2 = {
    cidr_block = "10.0.7.0/24"
    az         = "us-east-1b"
  }
}

# public_instance_type = "t2.micro"

# private_instance_type = "t2.micro"

//terraform plan -var-file sbx.tfvars

public_subnet_cidr = {
  public_subnet_1 = {
    cidr_block = "10.0.0.0/24"
    az         = "us-east-1a"
  }
  public_subnet_2 = {
    cidr_block = "10.0.2.0/24"
    az         = "us-east-1b"
  }
}

dns_name = "cmcloudlab730.info"

subject_alternative_names = ["*.cmcloudlab730.info"]