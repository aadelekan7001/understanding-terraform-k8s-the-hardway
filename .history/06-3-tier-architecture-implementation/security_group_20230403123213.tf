# //CREATE SG FOR ALB

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg-${terraform.workspace}"
  description = "Allow port 80/443 inbound traffic"
  vpc_id      = local.vpc_id

  ingress {
    description = "allow ingress on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    description = "allow ingress on port 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-sg-${terraform.workspace}"
  }
}

//create a private SG



resource "aws_security_group" "apps" {
  name        = "app1-${terraform.workspace}"
  description = "Allow SSH inbound traffic"
  vpc_id      = local.vpc_id
  depends_on = [aws_security_group.alb_sg]

  ingress {
    description     = "allow ingress on port 80"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${aws_security_group.alb_sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "app1-${terraform.workspace}"
  }
}


// create resgistration app SG
resource "aws_security_group" "registration_app" {
  name        = "registration-app-sg-${terraform.workspace}"
  description = "Allow traffic from ALB sg id"
  vpc_id      = local.vpc_id

  ingress {
    description     = "allow ingress on port 8080"
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = ["${aws_security_group.alb_sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "registration-app-sg-${terraform.workspace}"
  }
}

# // create registration app SG rule
# resource "aws_security_group_rule" "allow_ssh_from_bastion_sg" {
#   description = "allow port 80 on lb sg"
#   type        = "ingress"
#   from_port   = 8080
#   to_port     = 8080
#   protocol    = "tcp"

#   security_group_id        = aws_security_group.registration_app.id
#   source_security_group_id = aws_security_group.alb_sg.id // reference the source IG.
# }

// CREATE A DATABASE SECURITY GROUP

resource "aws_security_group" "database_security_group" {
  name        = "mysql-sg-${terraform.workspace}"
  description = "Allow traffic from registration app sg id"
  vpc_id      = local.vpc_id



  ingress {
    description     = "allow ingress on port 3306"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.registration_app.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mysql-sg-${terraform.workspace}"
  }
}

# // create registration app SG rule
# resource "aws_security_group_rule" "mysql_inbound_sg" {
#   description = "allow port 80 on lb sg"
#   type        = "ingress"
#   from_port   = 3306
#   to_port     = 3306
#   protocol    = "tcp"

#   security_group_id        = aws_security_group.database_security_group.id
#   source_security_group_id = aws_security_group.registration_app.id // reference the source IG.
# }