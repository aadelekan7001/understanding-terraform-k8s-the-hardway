variable "vpc_cidr" {
  type = string


}

variable "private_subnet_cidr" {
  type        = map(any)
  description = "objects of public subnets to be created"

}

variable "public_subnet_cidr" {
  type        = map(any)
  description = "objects of public subnets to be created"
  # default = {
  #   public_subnet_1 = {
  #     cidr_block = "10.0.0.0/24"
  #     az         = "us-east-1a"
  #   }

  #   public_subnet_2 = {
  #     cidr_block = "10.0.2.0/24"
  #     az         = "us-east-1b"
  #   }
  # }

}

variable "database_subnet_cidr" {
  type        = map(any)
  description = "objects of public subnets to be created"

}

variable "public_instance_type" {
  type        = string
  description = "public instance type"
  default     = "t2.micro"
}

variable "private_instance_type" {
  type        = string
  description = "private instance type"
  default     = "t2.micro"
}

variable "registration_instance_type"{
  default = "t2.xlarge"
}