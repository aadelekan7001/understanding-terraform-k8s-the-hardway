resource "aws_lb" "this" {
  name               = "web-alb-${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  //subnets            = local.pub_sub[keys(local.pub_sub)[count.index]].id
  subnets = [for i in aws_subnet.public_subnet : i.id]




  tags = {
    Name = "web-al${terraform.workspace}"
  }
}


resource "aws_lb_target_group" "app1" {
  name        = "app1-tg-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    unhealthy_threshold = 3
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app1/index.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }
}



resource "aws_lb_target_group" "app2" {
  name        = "app2-tg-${terraform.workspace}"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    unhealthy_threshold = 3
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/app2/index.html"
    matcher             = "200-399"
    port                = "traffic-port"
  }
}


resource "aws_lb_target_group" "registration_app" {
  name        = "registration-app-tg-${terraform.workspace}"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = local.vpc_id
  target_type = "instance"

  health_check {
    unhealthy_threshold = 3
    healthy_threshold   = 3
    interval            = 30
    protocol            = "HTTP"
    path                = "/login"
    matcher             = "200-399"
    port                = "traffic-port"
  }
}



resource "aws_lb_target_group_attachment" "app1" {
  count            = length(aws_instance.app_1)
  target_group_arn = aws_lb_target_group.app1.arn
  target_id        = aws_instance.app_1[count.index].id
  port             = 80
}

resource "aws_lb_target_group_attachment" "app2" {
  count            = length(aws_instance.app_2)
  target_group_arn = aws_lb_target_group.app2.arn
  target_id        = aws_instance.app_2[count.index].id
  port             = 80
}


resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.this.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}



resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.this.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  #certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }

  }
}