

resource "aws_db_instance" "registration_app_db" {
  allocated_storage    = 20
  db_name              = "webappdb"  #application team (webappdb)
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  username             = "foo"
  password             = "foobarbaz"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}