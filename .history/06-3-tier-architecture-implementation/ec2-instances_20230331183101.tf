// create public ec2 instance
resource "aws_instance" "app_1" {
  count = length(local.priv_sub)

  ami                  = data.aws_ami.ami.id
  instance_type        = var.public_instance_type
  subnet_id            = local.priv_sub[keys(local.priv_sub)[count.index]].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("${path.module}/templates/app1.sh")
  #subnet_id       = aws_subnet.public_subnet[0].id
  #key_name        = "koji"
  #security_groups = [aws_security_group.bastion_sg.id]
  security_groups = [aws_security_group.static_sg.id] #80 from ALB

  tags = {
    Name = "app1-${terraform.workspace}"
  }
}



# // create PRIVATE ec2 instance
resource "aws_instance" "app_2" {
  count                = length(local.priv_sub)
  ami                  = data.aws_ami.ami.id
  instance_type        = var.private_instance_type
  subnet_id            = local.priv_sub[keys(local.priv_sub)[count.index]].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("./templates/app2.sh")
  #key_name        = "koji"
  #security_groups = [aws_security_group.private_sg.id]
  security_groups = [aws_security_group.static_sg.id] #80 from ALB

  tags = {
    Name = "app2-${terraform.workspace}"
  }
}




# // create registration APP
resource "aws_instance" "registration-app" {

  depends_on = [
    
  ]
  count                = 2
  ami                  = data.aws_ami.ami.id
  instance_type        = var.registration_instance_type
  subnet_id            = local.priv_sub[keys(local.priv_sub)[count.index]].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  security_groups = [aws_security_group.registration_app.id] #8080 from ALB

  user_data            = templatefile("${path.module}/templates/registration_app.tmpl",
  {
    application_version = "v1.2.01"
     hostname = "" // datasource to pull
    port = "" // datasource to pull
    db_name = ""  // datasource to pull
    db_username = ""  // datasource to pull
    db_password = ""  // datasource to pull
  }
   


  
  
) #userdata #javafile
  #key_name        = "koji"
  #security_groups = [aws_security_group.private_sg.id]
  
  tags = {
    Name = "registration-app-${terraform.workspace}"
  }
}