resource "aws_lb" "this" {
  name               = "web-al${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  //subnets            = local.pub_sub[keys(local.pub_sub)[count.index]].id
  subnets = aws_subnet.public_subnet.*.id

  


  tags = {
    Name = "web-al${terraform.workspace}"
  }
}