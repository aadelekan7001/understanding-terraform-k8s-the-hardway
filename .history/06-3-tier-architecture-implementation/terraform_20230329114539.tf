terraform {
  required_version = ">=1.1.0"

  backend "s3" {
    bucket         = "04-tf-state-aadelekan"
    key            = "path/env"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.57.0"
    }
  }
}