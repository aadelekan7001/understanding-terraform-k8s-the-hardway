resource "random_password" "password" {
  length           = 16
  special          = false
  
}

locals {
  db_credentials = {
      endpoint = aws_db_instance.registration_app_db.address
      db_username = var.db_username
      db_name = var.db_name
  }
  
}




resource "aws_db_instance" "registration_app_db" {
  allocated_storage    = 20
  db_name              = var.db_name  #application team (webappdb)
  engine               = "mysql"

  instance_class       = var.instance_class
  username             = var.db_username
  password             = random_password.password.result
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
}