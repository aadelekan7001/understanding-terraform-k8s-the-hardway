resource "aws_lb" "this" {
  name               = "web-al${terraform.workspace}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  //subnets            = local.pub_sub[keys(local.pub_sub)[count.index]].id
  subnets = aws_subnet.public_subnet.*.id

  


  tags = {
    Name = "web-al${terraform.workspace}"
  }
}


resource "aws_lb_target_group" "app1" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}



resource "aws_lb_target_group" "app2" {
  name     = "tf-example-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}

