
locals {
  vpc_id = aws_vpc.this.id
  az     = data.aws_availability_zones.available.names
  pub_sub = aws_subnet.public_subnet
}



resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "kojitechs-vpc-${terraform.workspace}"
  }
}



resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id
  tags = {
    Name = "kojitechs-gw-${terraform.workspace}"
  }
}







resource "aws_subnet" "public_subnet" {
    for_each = var.public_subnet_cidr
  vpc_id                  = local.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = true

  tags = {
    Name = "each.key-${terraform.workspace}"
  }
}


resource "aws_subnet" "private_subnet" {
    for_each = var.private_subnet_cidr
  vpc_id                  = local.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = false

  tags = {
    Name = "each.key-${terraform.workspace}"
  }
}

resource "aws_subnet" "database_subnet" {
    for_each = var.database_subnet_cidr
  vpc_id                  = local.vpc_id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.az
  map_public_ip_on_launch = false

  tags = {
    Name = "each.key-${terraform.workspace}"
  }
}



// CREATE ROUTE TABLE
resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public-route-table"
  }
}


// create route table association
resource "aws_route_table_association" "rt_association" {
  count = length(aws_subnet.public_subnet)

  subnet_id      = local.pub_sub[keys(local.pub_sub)[count.index]].id
  route_table_id = aws_route_table.public_route_table.id
}


# # resource "aws_route_table_association" "public_subnet_2" {
# #   subnet_id      = aws_subnet.public_subnet_2.id
# #   route_table_id = aws_route_table.public_route_table.id
# # }


# // create default RT

# resource "aws_default_route_table" "this" {
#   default_route_table_id = aws_vpc.this.default_route_table_id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_nat_gateway.this.id
#   }


#   tags = {
#     Name = "this"
#   }
# }


# // CREATE A NAT GATEWAY
# resource "aws_nat_gateway" "this" {
#   depends_on    = [aws_internet_gateway.gw]
#   allocation_id = aws_eip.eip.id
#   subnet_id     = aws_subnet.public_subnet["public_subnet_1"].id

#   tags = {
#     Name = "gw-NAT"
#   }
# }





#   # To ensure proper ordering, it is recommended to add an explicit dependency
#   # on the Internet Gateway for the VPC.
#   // nat gateways need elastic IP

# # }


# // create EIP for NAT

# resource "aws_eip" "eip" {
#   depends_on = [
#     aws_internet_gateway.gw
#   ]
#   vpc = true
# }