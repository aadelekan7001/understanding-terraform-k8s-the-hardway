# locals {
#   vpc_id = aws_vpc.this.id
#   az     = data.aws_availability_zones.available.names
# }



# resource "aws_vpc" "this" {
#   cidr_block           = var.vpc_cidr
#   enable_dns_support   = true
#   enable_dns_hostnames = true

#   tags = {
#     Name = "kojitechs-vpc-${terraform.workspace}"
#   }
# }



# resource "aws_internet_gateway" "gw" {
#   vpc_id = local.vpc_id
#   tags = {
#     Name = "kojitechs-gw-${terraform.workspace}"
#   }
# }



# resource "aws_subnet" "public_subnet" {
#   count                   = length(var.public_subnet_cidr)
#   vpc_id                  = local.vpc_id
#   cidr_block              = var.public_subnet_cidr[count.index]
#   availability_zone       = local.az[count.index]
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "public-subnet-${count.index + 1}-${terraform.workspace}"
#   }
# }

# resource "aws_subnet" "public_subnet_2" {
#   vpc_id     = local.vpc_id
#   cidr_block = "10.0.2.0/24"
#   availability_zone = "us-east-1b"
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "public-subnet-2-${terraform.workspace}"
#   }
# }

# resource "aws_subnet" "private_subnet" {
#   count                   = length(var.private_subnet_cidr)
#   vpc_id                  = local.vpc_id
#   cidr_block              = var.private_subnet_cidr[count.index]
#   availability_zone       = local.az[count.index]
#   map_public_ip_on_launch = false

#   tags = {
#     Name = "private-subnet-${count.index + 1}-${terraform.workspace}"
#   }
# }
# resource "aws_subnet" "private_subnet_2" {
#   vpc_id     = local.vpc_id
#   cidr_block = "10.0.3.0/24"
#   availability_zone = "us-east-1b"

#   tags = {
#     Name = "private-subnet-1-${terraform.workspace}"
#   }
# }
# resource "aws_subnet" "database_subnet" {
#   count                   = length(var.database_subnet_cidr)
#   vpc_id                  = local.vpc_id
#   cidr_block              = var.database_subnet_cidr[count.index]
#   availability_zone       = local.az[count.index]
#   map_public_ip_on_launch = false

#   tags = {
#     Name = "database-subnet-1-${count.index + 1}-${terraform.workspace}"
#   }
# }
# resource "aws_subnet" "database_subnet_2" {
#   vpc_id     = local.vpc_id
#   cidr_block = "10.0.7.0/24"
#   availability_zone = "us-east-1b"

#   tags = {
#     Name = "database-subnet-2-${terraform.workspace}"
#   }
# }


//create route table
# resource "aws_route_table" "public_route_table" {
#   vpc_id = local.vpc_id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.gw.id
#   }

#   tags = {
#     Name = "public-route-table"
#   }
# }

# // create route table association
# resource "aws_route_table_association" "rt_association" {
#   count = length(var.public_subnet_cidr)

#   subnet_id      = aws_subnet.public_subnet.*.id[count.index]
#   route_table_id = aws_route_table.public_route_table.id
# }

# resource "aws_route_table_association" "public_subnet_2" {
#   subnet_id      = aws_subnet.public_subnet_2.id
#   route_table_id = aws_route_table.public_route_table.id
# }
// create default RT

# resource "aws_default_route_table" "this" {
#   default_route_table_id = aws_vpc.this.default_route_table_id

#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_nat_gateway.this.id
#   }


#   tags = {
#     Name = "this"
#   }
# }

// create a nat gateway

# resource "aws_nat_gateway" "this" {
#   depends_on    = [aws_internet_gateway.gw]
#   allocation_id = aws_eip.eip.id
#   subnet_id     = aws_subnet.public_subnet[0].id

#   tags = {
#     Name = "gw-NAT"
#   }

# To ensure proper ordering, it is recommended to add an explicit dependency
# on the Internet Gateway for the VPC.
// nat gateways need elastic IP

# }


// create EIP for NAT

# resource "aws_eip" "eip" {
#   depends_on = [
#     aws_internet_gateway.gw
#   ]
#   vpc = true
# }