resource "aws_s3_bucket" "this" {
  count = length(var.bucket_name)
  bucket = var.bucket_name[count.index]

 lifecycle {
   prevent_destroy = true
 }
}


# resource "aws_s3_bucket_acl" "example" {
#   bucket = aws_s3_bucket.this.id
#   acl    = "private"
# }

resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "terraform-lock"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID" #important

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true
  }
}